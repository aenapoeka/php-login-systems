<?php
    // Compatible with a server without nd_mysqli installed
    // PHP 7.1

if (isset($_POST['login-btn'])){
    require 'dbh.php';
    $username = $_POST['username'];
    $password = $_POST['password'];

    if(empty($username) || empty($password)){
        header("Location: ../?error=emptyfields");
        exit();
    }
    else{

        // SQL-template, username=?; on placeholder
        $sql = "SELECT id, username, password FROM users WHERE username=?;";

        // Prepared statement
        $stmt = mysqli_stmt_init($conn);

        // Testataan onnistuuko statement
        if(!mysqli_stmt_prepare($stmt, $sql)){
            header("Location: ../?error=sqlerror");
            exit();
        }
        else{
            // Parametrit placeholdereihin kiinni, 
            // eli 'WHERE username=?;' kohtaan bindataan string-tyypin $username
            mysqli_stmt_bind_param($stmt, "s", $username);

            mysqli_stmt_execute($stmt);

            mysqli_stmt_bind_result($stmt, $result_id, $result_username, $result_password);

            if($user = mysqli_stmt_fetch($stmt)){

                $passwordcheck = password_verify($password, $result_password);
                if($passwordcheck == false){
                    header("Location: ../?error=wrongpassword");
                    exit();
                }
                else if($passwordcheck == true){
                    session_start();
                    $_SESSION['userId'] = $result_id;
                    $_SESSION['username'] = $result_username;
                    header("Location: ../?login=success");
                    exit();
                }
            }
            else{
                header("Location: ../?error");
                exit();
            }
        }
    }
}
else{
    header("Location: ../index.php");
    exit();
}