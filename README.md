# PHP login systems

PHP-based login system for multiple users.
Allows backend data handling with SQL for logged in user.
2 different versions, older supports pre-7.1 PHP.

Uses prepared statements.